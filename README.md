# Gestion du personnel d’un point de vue comptabilité

### Auteurs: Leroy Laury / Quentin Dumont


Lien trello: https://trello.com/b/o618g6nU/gestion-personnel-pdv-compta

Lien google sheets: https://docs.google.com/spreadsheets/d/1CFDAtWwjvbPItlujtpY6PGnH51Ka6QalWM-9HDkHtdQ/edit#gid=0

#### Sous projet du logiciel de gestion d'université.

L'objectif de ce projet est de gérer la partie comptable d'une université, notamment la paye, les absences, la date des paiements, le budget etc...