package entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Date;

import org.junit.jupiter.api.Test;

public class FicheDePaieEntityTest {
	@Test
	public void testGetterAndSetters() {
		FicheDePaieEntity ficheDePaie = new FicheDePaieEntity();

		// Setters
		ficheDePaie.setIdFicheDePaie(1);
		ficheDePaie.setIdEmploye(101);
		ficheDePaie.setSalaireBrut(5000);
		ficheDePaie.setCotisationsPatronales(1000);
		ficheDePaie.setRole("RH");

		Date date = Date.valueOf("2022-01-01");
		ficheDePaie.setDate(date);

		// Getters
		assertEquals(1, ficheDePaie.getIdFicheDePaie());
		assertEquals(Integer.valueOf(101), ficheDePaie.getIdEmploye());
		assertEquals(5000, ficheDePaie.getSalaireBrut());
		assertEquals(1000, ficheDePaie.getCotisationsPatronales());
		assertEquals("RH", ficheDePaie.getRole());
		assertEquals(date, ficheDePaie.getDate());
	}

}
