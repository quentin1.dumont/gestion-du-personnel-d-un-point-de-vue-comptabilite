package entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Date;

import org.junit.jupiter.api.Test;

public class FactureEntityTest {
    @Test
    public void testGetSetEmetteur() {
        FactureEntity facture = new FactureEntity();
        facture.setEmetteur("TestEmetteur");
        assertEquals("TestEmetteur", facture.getEmetteur());
    }
    @Test
    public void testGetSetPrix() {
        FactureEntity facture = new FactureEntity();
        facture.setPrix(100);
        assertEquals(100, facture.getPrix());
    }

    @Test
    public void testGetSetDate() {
        FactureEntity facture = new FactureEntity();
        Date date = new Date(System.currentTimeMillis());
        facture.setDate(date);
        assertEquals(date, facture.getDate());
    }

    @Test
    public void testGetSetDescription() {
        FactureEntity facture = new FactureEntity();
        facture.setDescription("TestDescription");
        assertEquals("TestDescription", facture.getDescription());
    }
}
