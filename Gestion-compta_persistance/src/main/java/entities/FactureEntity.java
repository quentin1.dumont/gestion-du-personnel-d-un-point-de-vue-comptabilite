package entities;

import jakarta.persistence.*;

import java.sql.Date;

@Entity
@Table(name = "facture")
public class FactureEntity implements java.io.Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_facture")
    private int idFacture;

    @Column(name = "emetteur")
    private String emetteur;

    @Column(name = "prix")
    private int prix;

    @Column(name = "date")
    private Date date;

    @Column(name = "description")
    private String description;

    public int getIdFacture() {
        return idFacture;
    }

    public String getEmetteur() {
        return emetteur;
    }

    public void setEmetteur(String emetteur) {
        this.emetteur = emetteur;
    }

    public int getPrix() {
        return prix;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public void setIdFacture(int idFacture) {
        this.idFacture = idFacture;
    }


    public void setPrix(int prix) {
        this.prix = prix;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
