package entities;

import jakarta.persistence.*;

import java.sql.Date;

@Entity
@Table(name = "fiche_de_paie")
public class FicheDePaieEntity implements java.io.Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_fiche_de_paie")
    private int idFicheDePaie;

    @Column(name = "id_employe")
    private Integer idEmploye;

    @Column(name = "salaire_brut")
    private int salaireBrut;


    @Column(name = "cotisations_patronales")
    private int cotisationsPatronales;

    @Column(name = "date")
    private Date date;

    @Column(name = "role")
    private String role;

    public int getIdFicheDePaie() {
        return idFicheDePaie;
    }

    public void setIdFicheDePaie(int idFicheDePaie) {
        this.idFicheDePaie = idFicheDePaie;
    }

    public Integer getIdEmploye() {
        return idEmploye;
    }

    public void setIdEmploye(Integer idEmploye) {
        this.idEmploye = idEmploye;
    }

    public int getSalaireBrut() {
        return salaireBrut;
    }

    public void setSalaireBrut(int salaireBrut) {
        this.salaireBrut = salaireBrut;
    }

    public int getCotisationsPatronales() {
        return cotisationsPatronales;
    }

    public void setCotisationsPatronales(int cotisationsPatronales) {
        this.cotisationsPatronales = cotisationsPatronales;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRole() { return role; }

    public void setRole(String role) { this.role = role; }
}
