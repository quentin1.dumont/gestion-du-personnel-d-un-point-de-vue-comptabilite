package repositories;

import entities.FactureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;


@Repository
public interface FactureRepository extends JpaRepository<FactureEntity, Integer> {

    List<FactureEntity> findByEmetteur(String emetteur);
    List<FactureEntity> findByPrix(int prix);
    List<FactureEntity> findByDate(Date date);
    List<FactureEntity> findByPrixIsGreaterThan(int prix);
    List<FactureEntity> findByPrixIsLessThan(int prix);
    @Query("SELECT f FROM FactureEntity f WHERE MONTH(f.date) = :month")
    List<FactureEntity> findByDateMonth(@Param("month") int month);

    @Query("SELECT f FROM FactureEntity f WHERE MONTH(f.date) = :month AND YEAR(f.date) = :year")
    List<FactureEntity> findByDateMonthYear(@Param("month") int month, @Param("year") int year);

    @Query("SELECT f FROM FactureEntity f WHERE YEAR(f.date) = :year")
    List<FactureEntity> findByDateYear(@Param("year") int year);

    @Query("SELECT f FROM FactureEntity f WHERE f.date BETWEEN :date1 AND :date2")
    List<FactureEntity> findByDateBetween(@Param("date1") Date date1, @Param("date2") Date date2);
}
