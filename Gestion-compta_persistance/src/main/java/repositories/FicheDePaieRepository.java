package repositories;

import entities.FicheDePaieEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

@Repository
public interface FicheDePaieRepository extends JpaRepository<FicheDePaieEntity, Integer> {
    List<FicheDePaieEntity> findAllByIdEmployeOrderByDateDesc(int idEmploye);
    FicheDePaieEntity findByIdEmployeOrderByDateDesc(int idEmploye);
    List<FicheDePaieEntity> findAllByDate(Date date);
    List<FicheDePaieEntity> findAllBySalaireBrutGreaterThan(int salaireBrut);
    List<FicheDePaieEntity> findAllBySalaireBrutLessThan(int salaireBrut);

    @Transactional
    void deleteAllByIdEmploye(int idEmploye);

}
