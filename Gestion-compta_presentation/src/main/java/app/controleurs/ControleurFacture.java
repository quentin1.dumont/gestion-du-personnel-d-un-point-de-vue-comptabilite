package app.controleurs;

import dtos.FactureDTO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import services.factures.IServiceFacture;

import java.util.List;

@RestController
@RequestMapping("/factures")
@Tag(name = "Facture", description = "Opérations pour les factures")
@CrossOrigin(origins = "http://localhost:5173")
public class ControleurFacture {

    private final IServiceFacture serviceFacture;

    public ControleurFacture(IServiceFacture serviceFacture) {
        this.serviceFacture = serviceFacture;
    }

    @GetMapping("/")
    @Operation(summary = "Obtenir toutes les factures", description = "Obtenir toutes les factures", tags = { "Facture" })
    public ResponseEntity<List<FactureDTO>> obtenirFacture() {
        return ResponseEntity.ok(serviceFacture.obtenirFacture());
    }

    @GetMapping("/{id}")
    @Operation(summary = "Obtenir une facture par son id", description = "Obtenir une facture par son id", tags = { "Facture" })
    public ResponseEntity<FactureDTO> obtenirFactureParId(@PathVariable int id) {
        return ResponseEntity.ok(serviceFacture.obtenirFactureParId(id));
    }

    @PostMapping("/")
    @Operation(summary = "Ajouter une facture", description = "Ajouter une facture", tags = { "Facture" })
    public ResponseEntity<FactureDTO> ajouterFacture(@RequestBody FactureDTO facture) {
        serviceFacture.ajouterFacture(facture);
        return ResponseEntity.ok(facture);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Modifier une facture", description = "Modifier une facture", tags = { "Facture" })
    public ResponseEntity<FactureDTO> modifierFacture(@PathVariable int id, @RequestBody FactureDTO facture) {
        serviceFacture.modifierFacture(id, facture);
        return ResponseEntity.ok(facture);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Supprimer une facture", description = "Supprimer une facture", tags = { "Facture" })
    public ResponseEntity<Integer> supprimerFacture(@PathVariable int id) {
        serviceFacture.supprimerFacture(id);
        return ResponseEntity.ok(id);
    }

    @GetMapping("/emetteur/{emetteur}")
    @Operation(summary = "Obtenir une facture par son émetteur", description = "Obtenir une facture par son émetteur", tags = { "Facture" })
    public ResponseEntity<List<FactureDTO>> obtenirFactureParEmetteur(@PathVariable String emetteur) {
        return ResponseEntity.ok(serviceFacture.obtenirFactureParEmetteur(emetteur));
    }

}
