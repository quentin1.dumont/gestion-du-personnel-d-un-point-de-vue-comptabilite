package app.controleurs;

import dtos.FicheDePaieDTO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import services.fichesDePaie.IServiceFicheDePaie;

import java.sql.Date;
import java.util.List;

@RestController
@RequestMapping("/fichesDePaie")
@Tag(name = "Fiche de paie", description = "Opérations pour les fiches de paie")
@CrossOrigin(origins = "http://localhost:5173")
public class ControlleurFicheDePaie {

    private final IServiceFicheDePaie serviceFicheDePaie;

    public ControlleurFicheDePaie(IServiceFicheDePaie serviceFicheDePaie) {
        this.serviceFicheDePaie = serviceFicheDePaie;
    }

    @GetMapping("/")
    @Operation(summary = "Obtenir toutes les fiches de paie", description = "Obtenir toutes les fiches de paie", tags = { "Fiche de paie" })
    public ResponseEntity<List<FicheDePaieDTO>> obtenirFicheDePaie() {
        return ResponseEntity.ok(serviceFicheDePaie.obtenirFicheDePaie());
    }

    @GetMapping("/{id}")
    @Operation(summary = "Obtenir une fiche de paie par son id", description = "Obtenir une fiche de paie par son id", tags = { "Fiche de paie" })
    public ResponseEntity<FicheDePaieDTO> obtenirFicheDePaieParId(@PathVariable int id) {
        return ResponseEntity.ok(serviceFicheDePaie.obtenirFicheDePaieParId(id));
    }

    @PostMapping("/")
    @Operation(summary = "Ajouter une fiche de paie", description = "Ajouter une fiche de paie", tags = { "Fiche de paie" })
    public ResponseEntity<FicheDePaieDTO> ajouterFicheDePaie(@RequestBody FicheDePaieDTO ficheDePaie) {
        serviceFicheDePaie.ajouterFicheDePaie(ficheDePaie);
        return ResponseEntity.ok(ficheDePaie);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Modifier une fiche de paie", description = "Modifier une fiche de paie", tags = { "Fiche de paie" })
    public ResponseEntity<FicheDePaieDTO> modifierFicheDePaie(@PathVariable int id, @RequestBody FicheDePaieDTO ficheDePaie) {
        serviceFicheDePaie.modifierFicheDePaie(id, ficheDePaie);
        return ResponseEntity.ok(ficheDePaie);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Supprimer une fiche de paie", description = "Supprimer une fiche de paie", tags = { "Fiche de paie" })
    public ResponseEntity<Integer> supprimerFicheDePaie(@PathVariable int id) {
        serviceFicheDePaie.supprimerFicheDePaie(id);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/employe/{idEmploye}/supprimer")
    @Operation(summary = "Supprimer les fiches de paie d'un employé", description = "Supprimer les fiches de paie d'un employé", tags = { "Fiche de paie" })
    public ResponseEntity<Integer> supprimerFicheDePaieParEmploye(@PathVariable int idEmploye) {
        serviceFicheDePaie.supprimerFicheDePaieParEmploye(idEmploye);
        return ResponseEntity.ok(idEmploye);
    }

    @GetMapping("/employe/{idEmploye}")
    @Operation(summary = "Obtenir les fiches de paie d'un employé", description = "Obtenir les fiches de paie d'un employé", tags = { "Fiche de paie" })
    public ResponseEntity<List<FicheDePaieDTO>> obtenirFicheDePaieParEmploye(@PathVariable int idEmploye) {
        return ResponseEntity.ok(serviceFicheDePaie.obtenirFicheDePaieParEmploye(idEmploye));
    }

    @GetMapping("/date/{date}")
    @Operation(summary = "Obtenir les fiches de paie de telle date", description = "Obtenir les fiches de paie de telle date", tags = { "Fiche de paie" })
    public ResponseEntity<List<FicheDePaieDTO>> obtenirFicheDePaieParDate(@PathVariable Date date) {
        return ResponseEntity.ok(serviceFicheDePaie.obtenirFicheDePaieParDate(date));
    }

    @GetMapping("/salaireBrutPlusGrandQue/{salaireBrut}")
    @Operation(summary = "Obtenir les fiches de paie dont le salaire brut est plus grand que X", description = "Obtenir les fiches de paie dont le salaire brut est plus grand que X", tags = { "Fiche de paie" })
    public ResponseEntity<List<FicheDePaieDTO>> obtenirFicheDePaieParSalaireBrutPlusGrandQue(@PathVariable int salaireBrut) {
        return ResponseEntity.ok(serviceFicheDePaie.obtenirFicheDePaieParSalaireBrutPlusGrandQue(salaireBrut));
    }

    @GetMapping("/salaireBrutPlusPetitQue/{salaireBrut}")
    @Operation(summary = "Obtenir les fiches de paie dont le salaire brut est plus petit que X", description = "Obtenir les fiches de paie dont le salaire brut est plus petit que X", tags = { "Fiche de paie" })
    public ResponseEntity<List<FicheDePaieDTO>> obtenirFicheDePaieParSalaireBrutPlusPetitQue(@PathVariable int salaireBrut) {
        return ResponseEntity.ok(serviceFicheDePaie.obtenirFicheDePaieParSalaireBrutPlusPetitQue(salaireBrut));
    }

    @GetMapping("/employe/{idEmploye}/derniereFiche")
    @Operation(summary = "Obtenir la dernière fiche de paie d'un employé", description = "Obtenir la dernière fiche de paie d'un employé", tags = { "Fiche de paie" })
    public ResponseEntity<FicheDePaieDTO> obtenirDerniereFicheDePaieParEmploye(@PathVariable int idEmploye) {
        return ResponseEntity.ok(serviceFicheDePaie.obtenirDerniereFicheDePaieParEmploye(idEmploye));
    }

}
