package service;

import dtos.FicheDePaieDTO;
import entities.FicheDePaieEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repositories.FicheDePaieRepository;
import services.fichesDePaie.ServiceFicheDePaie;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ServiceFicheDePaieTest {

    @InjectMocks
    private ServiceFicheDePaie serviceFicheDePaie;

    @Mock
    private FicheDePaieRepository ficheDePaieRepository;

    @Mock
    private FicheDePaieEntity ficheDePaieEntity1 = new FicheDePaieEntity();

    @Mock
    private FicheDePaieEntity ficheDePaieEntity2 = new FicheDePaieEntity();

    @BeforeEach
    public void setUp() {
        serviceFicheDePaie = new ServiceFicheDePaie(ficheDePaieRepository);
        ficheDePaieEntity1.setIdFicheDePaie(5);
        ficheDePaieEntity1.setIdEmploye(101);
        ficheDePaieEntity1.setSalaireBrut(5000);
        ficheDePaieEntity1.setRole("RH");
        ficheDePaieEntity1.setCotisationsPatronales(1000);
        Date date = Date.valueOf("2022-01-01");
        ficheDePaieEntity1.setDate(date);
        ficheDePaieEntity2.setIdFicheDePaie(3);
        ficheDePaieEntity2.setIdEmploye(102);
        ficheDePaieEntity2.setSalaireBrut(6000);
        ficheDePaieEntity2.setRole("RH");
        ficheDePaieEntity2.setCotisationsPatronales(2000);
        Date date2 = Date.valueOf("2022-01-02");
        ficheDePaieEntity2.setDate(date2);
    }

    @Test
    public void testObtenirFicheDePaieParId() {
        when(ficheDePaieRepository.findById(1)).thenReturn(Optional.of(ficheDePaieEntity1));
        FicheDePaieDTO ficheDePaieDTO = serviceFicheDePaie.obtenirFicheDePaieParId(1);
        assertNotNull(ficheDePaieDTO);
        assertEquals(ficheDePaieDTO.getIdFicheDePaie(), ficheDePaieEntity1.getIdFicheDePaie());
    }

    @Test
    public void testObtenirFichesDePaie() {
        List<FicheDePaieEntity> ficheDePaieEntities = new ArrayList<>();
        ficheDePaieEntities.add(ficheDePaieEntity1);
        ficheDePaieEntities.add(ficheDePaieEntity2);
        when(ficheDePaieRepository.findAll()).thenReturn(ficheDePaieEntities);
        List<FicheDePaieDTO> ficheDePaieDTOs = serviceFicheDePaie.obtenirFicheDePaie();
        assertNotNull(ficheDePaieDTOs);
        assertEquals(ficheDePaieDTOs.size(), ficheDePaieEntities.size());
    }

    @Test
    public void testAjouterFicheDePaie() {
        FicheDePaieDTO ficheDePaieDTO = new FicheDePaieDTO();
        ficheDePaieDTO.setIdFicheDePaie(1);
        serviceFicheDePaie.ajouterFicheDePaie(ficheDePaieDTO);
        verify(ficheDePaieRepository, times(1)).save(any(FicheDePaieEntity.class));
    }

    @Test
    public void testSupprimerFicheDePaie() {
        serviceFicheDePaie.supprimerFicheDePaie(1);
        verify(ficheDePaieRepository, times(1)).deleteById(1);
    }

    @Test
    public void testModifierFicheDePaie() {
        FicheDePaieDTO ficheDePaieDTO = new FicheDePaieDTO();
        ficheDePaieDTO.setIdFicheDePaie(1);
        when(ficheDePaieRepository.existsById(1)).thenReturn(true);
        when(ficheDePaieRepository.findById(1)).thenReturn(Optional.of(ficheDePaieEntity1));
        serviceFicheDePaie.modifierFicheDePaie(1, ficheDePaieDTO);
        verify(ficheDePaieRepository, times(1)).save(any(FicheDePaieEntity.class));
    }

    @Test
       public void testObtenirFicheDePaieParEmploye() {
        List<FicheDePaieEntity> ficheDePaieEntities = new ArrayList<>();
        ficheDePaieEntities.add(ficheDePaieEntity1);
        when(ficheDePaieRepository.findAllByIdEmployeOrderByDateDesc(101)).thenReturn(ficheDePaieEntities);
        List<FicheDePaieDTO> ficheDePaieDTOs = serviceFicheDePaie.obtenirFicheDePaieParEmploye(101);
        assertNotNull(ficheDePaieDTOs);
        assertEquals(ficheDePaieDTOs.size(), ficheDePaieEntities.size());
    }

    @Test
    public void testObtenirFicheDePaieParDate() {
        List<FicheDePaieEntity> ficheDePaieEntities = new ArrayList<>();
        ficheDePaieEntities.add(ficheDePaieEntity1);
        when(ficheDePaieRepository.findAllByDate(Date.valueOf("2022-01-01"))).thenReturn(ficheDePaieEntities);
        List<FicheDePaieDTO> ficheDePaieDTOs = serviceFicheDePaie.obtenirFicheDePaieParDate(Date.valueOf("2022-01-01"));
        assertNotNull(ficheDePaieDTOs);
        assertEquals(ficheDePaieDTOs.size(), ficheDePaieEntities.size());
    }

    @Test
    public void testObtenirFicheDePaieParSalaireBrutPlusGrandQue() {
        List<FicheDePaieEntity> ficheDePaieEntities = new ArrayList<>();
        ficheDePaieEntities.add(ficheDePaieEntity1);
        when(ficheDePaieRepository.findAllBySalaireBrutGreaterThan(5000)).thenReturn(ficheDePaieEntities);
        List<FicheDePaieDTO> ficheDePaieDTOs = serviceFicheDePaie.obtenirFicheDePaieParSalaireBrutPlusGrandQue(5000);
        assertNotNull(ficheDePaieDTOs);
        assertEquals(ficheDePaieDTOs.size(), ficheDePaieEntities.size());
    }

    @Test
    public void testObtenirFicheDePaieParSalaireBrutPlusPetitQue() {
        List<FicheDePaieEntity> ficheDePaieEntities = new ArrayList<>();
        ficheDePaieEntities.add(ficheDePaieEntity1);
        when(ficheDePaieRepository.findAllBySalaireBrutLessThan(5000)).thenReturn(ficheDePaieEntities);
        List<FicheDePaieDTO> ficheDePaieDTOs = serviceFicheDePaie.obtenirFicheDePaieParSalaireBrutPlusPetitQue(5000);
        assertNotNull(ficheDePaieDTOs);
        assertEquals(ficheDePaieDTOs.size(), ficheDePaieEntities.size());
    }

    @Test
    public void testObtenirDerniereFicheDePaieParEmploye() {
        when(ficheDePaieRepository.findByIdEmployeOrderByDateDesc(101)).thenReturn(ficheDePaieEntity1);
        FicheDePaieDTO ficheDePaieDTO = serviceFicheDePaie.obtenirDerniereFicheDePaieParEmploye(101);
        assertNotNull(ficheDePaieDTO);
        assertEquals(ficheDePaieDTO.getIdFicheDePaie(), ficheDePaieEntity1.getIdFicheDePaie());
    }

    @Test
    public void testSupprimerFicheDePaieParEmploye() {
        serviceFicheDePaie.supprimerFicheDePaieParEmploye(101);
        verify(ficheDePaieRepository, times(1)).deleteAllByIdEmploye(101);
    }




    @AfterEach
    public void tearDown() {
        serviceFicheDePaie = null;
    }

}
