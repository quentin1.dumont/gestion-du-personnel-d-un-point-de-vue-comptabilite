package  service;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import dtos.FactureDTO;
import entities.FactureEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repositories.FactureRepository;
import services.factures.ServiceFacture;

@ExtendWith(MockitoExtension.class)
public class ServiceFactureTest {

    @InjectMocks
    private ServiceFacture serviceFacture;

    @Mock
    private FactureRepository factureRepository;

    @Mock
    private FactureEntity factureEntity1 = new FactureEntity();

    @Mock
    private FactureEntity factureEntity2 = new FactureEntity();

    @BeforeEach
    public void setUp() {
        serviceFacture = new ServiceFacture(factureRepository);
        factureEntity1.setIdFacture(5);
        factureEntity1.setEmetteur("emetteur1");
        factureEntity1.setPrix(100);
        factureEntity1.setDate(Date.valueOf("2022-01-01"));
        factureEntity1.setDescription("description1");
        factureEntity2.setIdFacture(3);
        factureEntity2.setEmetteur("emetteur2");
        factureEntity2.setPrix(200);
        factureEntity2.setDate(Date.valueOf("2022-01-02"));
        factureEntity2.setDescription("description2");
    }

    @Test
    public void testObtenirFactureParId() {
        when(factureRepository.findById(1)).thenReturn(Optional.of(factureEntity1));
        FactureDTO factureDTO = serviceFacture.obtenirFactureParId(1);
        assertNotNull(factureDTO);
        assertEquals(factureDTO.getIdFacture(), factureEntity1.getIdFacture());
    }

    @Test
    public void testAjouterFacture() {
        FactureDTO factureDTO = new FactureDTO();
        factureDTO.setIdFacture(1);
        factureDTO.setEmetteur("emetteur1");
        factureDTO.setPrix(100);
        factureDTO.setDate(Date.valueOf("2022-01-01"));
        factureDTO.setDescription("description1");
        serviceFacture.ajouterFacture(factureDTO);
        verify(factureRepository, times(1)).save(any(FactureEntity.class));
    }

    @Test
    public void testSupprimerFacture() {
        serviceFacture.supprimerFacture(1);
        verify(factureRepository, times(1)).deleteById(1);
    }

    @Test
    public void testModifierFacture() {
        FactureDTO factureDTO = new FactureDTO();
        factureDTO.setIdFacture(1);
        factureDTO.setEmetteur("emetteur1");
        factureDTO.setPrix(100);
        factureDTO.setDate(Date.valueOf("2022-01-01"));
        factureDTO.setDescription("description1");
        when(factureRepository.findById(1)).thenReturn(Optional.of(factureEntity1));
        serviceFacture.modifierFacture(1, factureDTO);
        verify(factureRepository, times(1)).save(any(FactureEntity.class));
    }

    @Test
    public void testObtenirFacture() {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        factureEntityList.add(factureEntity1);
        factureEntityList.add(factureEntity2);
        when(factureRepository.findAll()).thenReturn(factureEntityList);
        List<FactureDTO> factureDTOList = serviceFacture.obtenirFacture();
        assertNotNull(factureDTOList);
        assertEquals(2, factureDTOList.size());
        assertEquals(factureDTOList.get(0).getIdFacture(), factureEntity1.getIdFacture());
        assertEquals(factureDTOList.get(1).getIdFacture(), factureEntity2.getIdFacture());
    }

    @Test
    public void testObtenirFactureParEmetteur() {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        factureEntityList.add(factureEntity1);
        when(factureRepository.findByEmetteur("emetteur1")).thenReturn(factureEntityList);
        List<FactureDTO> factureDTOList = serviceFacture.obtenirFactureParEmetteur("emetteur1");
        assertNotNull(factureDTOList);
        assertEquals(1, factureDTOList.size());
    }

    @Test
    public void testObtenirFactureParPrix() {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        factureEntityList.add(factureEntity1);
        when(factureRepository.findByPrix(100)).thenReturn(factureEntityList);
        List<FactureDTO> factureDTOList = serviceFacture.obtenirFactureParPrix(100);
        assertNotNull(factureDTOList);
        assertEquals(1, factureDTOList.size());
    }

    @Test
    public void testObtenirFactureParDate() {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        factureEntityList.add(factureEntity1);
        when(factureRepository.findByDate(Date.valueOf("2022-01-01"))).thenReturn(factureEntityList);
        List<FactureDTO> factureDTOList = serviceFacture.obtenirFactureParDate(Date.valueOf("2022-01-01"));
        assertNotNull(factureDTOList);
        assertEquals(1, factureDTOList.size());
    }

    @Test
    public void testObtenirFactureParPrixPlusGrandQue() {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        factureEntityList.add(factureEntity2);
        when(factureRepository.findByPrixIsGreaterThan(100)).thenReturn(factureEntityList);
        List<FactureDTO> factureDTOList = serviceFacture.obtenirFactureParPrixPlusGrandQue(100);
        assertNotNull(factureDTOList);
        assertEquals(1, factureDTOList.size());
    }

    @Test
    public void testObtenirFactureParPrixPlusPetitQue() {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        factureEntityList.add(factureEntity1);
        when(factureRepository.findByPrixIsLessThan(200)).thenReturn(factureEntityList);
        List<FactureDTO> factureDTOList = serviceFacture.obtenirFactureParPrixPlusPetitQue(200);
        assertNotNull(factureDTOList);
        assertEquals(1, factureDTOList.size());
    }

    @Test
    public void testObtenirFactureParDateMois() {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        factureEntityList.add(factureEntity1);
        when(factureRepository.findByDateMonth(1)).thenReturn(factureEntityList);
        List<FactureDTO> factureDTOList = serviceFacture.obtenirFactureParDateMois(1);
        assertNotNull(factureDTOList);
        assertEquals(1, factureDTOList.size());
    }

    @Test
    public void testObtenirFactureParDateMoisAnnee() {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        factureEntityList.add(factureEntity1);
        when(factureRepository.findByDateMonthYear(1, 2022)).thenReturn(factureEntityList);
        List<FactureDTO> factureDTOList = serviceFacture.obtenirFactureParDateMoisAnnee(1, 2022);
        assertNotNull(factureDTOList);
        assertEquals(1, factureDTOList.size());
    }

    @Test
    public void testObtenirFactureParDateAnnee() {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        factureEntityList.add(factureEntity1);
        when(factureRepository.findByDateYear(2022)).thenReturn(factureEntityList);
        List<FactureDTO> factureDTOList = serviceFacture.obtenirFactureParDateAnnee(2022);
        assertNotNull(factureDTOList);
        assertEquals(1, factureDTOList.size());
    }

    @Test
    public void testObtenirFactureParDateEntre() {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        factureEntityList.add(factureEntity1);
        when(factureRepository.findByDateBetween(Date.valueOf("2022-01-01"), Date.valueOf("2022-01-02"))).thenReturn(factureEntityList);
        List<FactureDTO> factureDTOList = serviceFacture.obtenirFactureParDateEntre(Date.valueOf("2022-01-01"), Date.valueOf("2022-01-02"));
        assertNotNull(factureDTOList);
        assertEquals(1, factureDTOList.size());
    }

    @Test
    public void testObtenirDerniereFactureParEmetteur() {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        factureEntityList.add(factureEntity1);
        factureEntityList.add(factureEntity2);
        when(factureRepository.findByEmetteur("emetteur1")).thenReturn(factureEntityList);
        FactureDTO factureDTO = serviceFacture.obtenirDerniereFactureParEmetteur("emetteur1");
        assertNotNull(factureDTO);
        assertEquals(factureDTO.getDate(), factureEntity1.getDate());
    }


    @AfterEach
    public void tearDown() {
        serviceFacture = null;

    }

}