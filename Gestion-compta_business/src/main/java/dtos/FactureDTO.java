package dtos;

import java.sql.Date;

public class FactureDTO implements java.io.Serializable{

    private int idFacture;

    private String emetteur;

    private int prix;

    private Date date;

    private String description;

    public int getIdFacture() {
        return idFacture;
    }

    public String getEmetteur() {
        return emetteur;
    }

    public void setEmetteur(String emetteur) {
        this.emetteur = emetteur;
    }

    public int getPrix() {
        return prix;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public void setIdFacture(int idFacture) {
        this.idFacture = idFacture;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
