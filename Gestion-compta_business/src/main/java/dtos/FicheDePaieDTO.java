package dtos;

import java.sql.Date;

public class FicheDePaieDTO implements java.io.Serializable{
    private int idFicheDePaie;

    private Integer idEmploye;

    private int salaireBrut;

    private int cotisationsPatronales;

    private Date date;

    private String role;

    public int getIdFicheDePaie() {
        return idFicheDePaie;
    }

    public void setIdFicheDePaie(int idFicheDePaie) {
        this.idFicheDePaie = idFicheDePaie;
    }

    public Integer getIdEmploye() {
        return idEmploye;
    }

    public void setIdEmploye(Integer idEmploye) {
        this.idEmploye = idEmploye;
    }

    public int getSalaireBrut() {
        return salaireBrut;
    }

    public void setSalaireBrut(int salaireBrut) {
        this.salaireBrut = salaireBrut;
    }

    public int getCotisationsPatronales() {
        return cotisationsPatronales;
    }

    public void setCotisationsPatronales(int cotisationsPatronales) {
        this.cotisationsPatronales = cotisationsPatronales;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRole() { return role; }

    public void setRole(String role) { this.role = role; }

}
