package services.factures;

import convertisseurs.ConvertisseurFacture;
import dtos.FactureDTO;
import entities.FactureEntity;
import org.springframework.stereotype.Service;
import repositories.FactureRepository;

import java.util.List;

@Service
public class ServiceFacture implements IServiceFacture {
    private final FactureRepository factureRepository;
    private final ConvertisseurFacture convertisseurFacture;

    public ServiceFacture(FactureRepository factureRepository) {
        this.convertisseurFacture = ConvertisseurFacture.getInstance();
        this.factureRepository = factureRepository;
    }

    public FactureDTO obtenirFactureParId(int id) {
        FactureEntity facture = factureRepository.findById(id).orElse(null);
        if (facture == null) return null;
        return convertisseurFacture.convertirVersDTO(facture);
    }

    public void ajouterFacture(FactureDTO facture) {
        factureRepository.save(convertisseurFacture.convertirVersEntity(facture));
    }

    public void supprimerFacture(int id) {
        factureRepository.deleteById(id);
    }

    @Override
    public void modifierFacture(int id, FactureDTO facture) {
        FactureEntity factureEntity = factureRepository.findById(id).orElse(null);
        if (factureEntity == null) throw new IllegalArgumentException("Facture non trouvée");
        factureEntity.setEmetteur(facture.getEmetteur());
        factureEntity.setPrix(facture.getPrix());
        factureEntity.setDate(facture.getDate());
        factureEntity.setDescription(facture.getDescription());
        factureRepository.save(factureEntity);
    }

    public List<FactureDTO> obtenirFacture() {
        return convertisseurFacture.convertirVersDTO(factureRepository.findAll());
    }

    public List<FactureDTO> obtenirFactureParEmetteur(String emetteur) {
        return convertisseurFacture.convertirVersDTO(factureRepository.findByEmetteur(emetteur));
    }

    public List<FactureDTO> obtenirFactureParPrix(int prix) {
        return convertisseurFacture.convertirVersDTO(factureRepository.findByPrix(prix));
    }

    public List<FactureDTO> obtenirFactureParDate(java.sql.Date date) {
        return convertisseurFacture.convertirVersDTO(factureRepository.findByDate(date));
    }

    public List<FactureDTO> obtenirFactureParPrixPlusGrandQue(int prix) {
        return convertisseurFacture.convertirVersDTO(factureRepository.findByPrixIsGreaterThan(prix));
    }

    public List<FactureDTO> obtenirFactureParPrixPlusPetitQue(int prix) {
        return convertisseurFacture.convertirVersDTO(factureRepository.findByPrixIsLessThan(prix));
    }

    public List<FactureDTO> obtenirFactureParDateMois(int month) {
        return convertisseurFacture.convertirVersDTO(factureRepository.findByDateMonth(month));
    }

    public List<FactureDTO> obtenirFactureParDateMoisAnnee(int month, int year) {
        return convertisseurFacture.convertirVersDTO(factureRepository.findByDateMonthYear(month, year));
    }

    public List<FactureDTO> obtenirFactureParDateAnnee(int year) {
        return convertisseurFacture.convertirVersDTO(factureRepository.findByDateYear(year));
    }

    public List<FactureDTO> obtenirFactureParDateEntre(java.sql.Date date1, java.sql.Date date2) {
        return convertisseurFacture.convertirVersDTO(factureRepository.findByDateBetween(date1, date2));
    }

    public FactureDTO obtenirDerniereFactureParEmetteur(String emetteur) {
        return convertisseurFacture.convertirVersDTO(factureRepository.findByEmetteur(emetteur).get(factureRepository.findByEmetteur(emetteur).size() - 1));
    }



}
