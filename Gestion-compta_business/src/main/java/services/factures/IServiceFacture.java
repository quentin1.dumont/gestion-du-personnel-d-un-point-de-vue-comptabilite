package services.factures;

import dtos.FactureDTO;

import java.sql.Date;
import java.util.List;

public interface IServiceFacture {
    void ajouterFacture(FactureDTO facture);
    void supprimerFacture(int id);
    void modifierFacture(int id, FactureDTO facture);
    List<FactureDTO> obtenirFacture();
    List<FactureDTO> obtenirFactureParEmetteur(String emetteur);
    List<FactureDTO> obtenirFactureParPrix(int prix);
    List<FactureDTO> obtenirFactureParDate(Date date);
    List<FactureDTO> obtenirFactureParPrixPlusGrandQue(int prix);
    List<FactureDTO> obtenirFactureParPrixPlusPetitQue(int prix);
    List<FactureDTO> obtenirFactureParDateMois(int month);
    List<FactureDTO> obtenirFactureParDateMoisAnnee(int month, int year);
    List<FactureDTO> obtenirFactureParDateAnnee(int year);
    List<FactureDTO> obtenirFactureParDateEntre(Date date1, Date date2);
    FactureDTO obtenirDerniereFactureParEmetteur(String emetteur);
    FactureDTO obtenirFactureParId(int id);
}
