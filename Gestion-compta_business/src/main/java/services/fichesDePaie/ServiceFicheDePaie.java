package services.fichesDePaie;

import dtos.FicheDePaieDTO;
import convertisseurs.ConvertisseurFicheDePaie;
import entities.FicheDePaieEntity;
import org.springframework.stereotype.Service;
import repositories.FicheDePaieRepository;

import java.sql.Date;
import java.util.List;

@Service
public class ServiceFicheDePaie implements IServiceFicheDePaie {
    private final FicheDePaieRepository ficheDePaieRepository;
    private final ConvertisseurFicheDePaie convertisseurFicheDePaie;

    public ServiceFicheDePaie(FicheDePaieRepository ficheDePaieRepository) {
        this.convertisseurFicheDePaie = ConvertisseurFicheDePaie.getInstance();
        this.ficheDePaieRepository = ficheDePaieRepository;
    }

    public FicheDePaieDTO obtenirFicheDePaieParId(int id) {
        FicheDePaieEntity ficheDePaie = ficheDePaieRepository.findById(id).orElse(null);
        if (ficheDePaie == null) return null;
        return convertisseurFicheDePaie.convertirVersDTO(ficheDePaie);
    }

    public void ajouterFicheDePaie(FicheDePaieDTO ficheDePaie) {
        ficheDePaieRepository.save(convertisseurFicheDePaie.convertirVersEntity(ficheDePaie));
    }

    public void supprimerFicheDePaie(int id) {
        ficheDePaieRepository.deleteById(id);
    }

    public void supprimerFicheDePaieParEmploye(int idEmploye) {
        ficheDePaieRepository.deleteAllByIdEmploye(idEmploye);
    }

    public void modifierFicheDePaie(int id, FicheDePaieDTO ficheDePaie) {
        if (!ficheDePaieRepository.existsById(id)) throw new IllegalArgumentException("La fiche de paie n'existe pas");
        FicheDePaieEntity ficheDePaieEntity = ficheDePaieRepository.findById(id).orElse(null);
        if (ficheDePaieEntity == null) throw new IllegalArgumentException("Fiche de paie non trouvée");
        ficheDePaieEntity.setIdEmploye(ficheDePaie.getIdEmploye());
        ficheDePaieEntity.setDate(ficheDePaie.getDate());
        ficheDePaieEntity.setSalaireBrut(ficheDePaie.getSalaireBrut());
        ficheDePaieEntity.setCotisationsPatronales(ficheDePaie.getCotisationsPatronales());
        ficheDePaieEntity.setRole(ficheDePaie.getRole());
        ficheDePaieRepository.save(ficheDePaieEntity);
    }

    public List<FicheDePaieDTO> obtenirFicheDePaie() {
        return convertisseurFicheDePaie.convertirVersDTO(ficheDePaieRepository.findAll());
    }

    public List<FicheDePaieDTO> obtenirFicheDePaieParEmploye(int idEmploye) {
        return convertisseurFicheDePaie.convertirVersDTO(ficheDePaieRepository.findAllByIdEmployeOrderByDateDesc(idEmploye));
    }

    public List<FicheDePaieDTO> obtenirFicheDePaieParDate(Date date) {
        return convertisseurFicheDePaie.convertirVersDTO(ficheDePaieRepository.findAllByDate(date));
    }

    public List<FicheDePaieDTO> obtenirFicheDePaieParSalaireBrutPlusGrandQue(int salaireBrut) {
        return convertisseurFicheDePaie.convertirVersDTO(ficheDePaieRepository.findAllBySalaireBrutGreaterThan(salaireBrut));
    }

    public List<FicheDePaieDTO> obtenirFicheDePaieParSalaireBrutPlusPetitQue(int salaireBrut) {
        return convertisseurFicheDePaie.convertirVersDTO(ficheDePaieRepository.findAllBySalaireBrutLessThan(salaireBrut));
    }

    public FicheDePaieDTO obtenirDerniereFicheDePaieParEmploye(int idEmploye) {
        if (ficheDePaieRepository.findByIdEmployeOrderByDateDesc(idEmploye) == null)
            throw new IllegalArgumentException("L'employé n'a pas de fiche de paie");
        return convertisseurFicheDePaie.convertirVersDTO(ficheDePaieRepository.findByIdEmployeOrderByDateDesc(idEmploye));
    }
}
