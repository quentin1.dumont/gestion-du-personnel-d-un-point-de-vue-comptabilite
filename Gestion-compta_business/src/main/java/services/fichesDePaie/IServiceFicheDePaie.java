package services.fichesDePaie;

import dtos.FicheDePaieDTO;

import java.sql.Date;
import java.util.List;

public interface IServiceFicheDePaie {
    public FicheDePaieDTO obtenirFicheDePaieParId(int id) ;

    public void ajouterFicheDePaie(FicheDePaieDTO ficheDePaie) ;

    public void supprimerFicheDePaie(int id) ;

    public void modifierFicheDePaie(int id, FicheDePaieDTO ficheDePaie) throws IllegalArgumentException;

    public List<FicheDePaieDTO> obtenirFicheDePaie() ;

    public List<FicheDePaieDTO> obtenirFicheDePaieParEmploye(int idEmploye) ;

    public List<FicheDePaieDTO> obtenirFicheDePaieParDate(Date date) ;

    public List<FicheDePaieDTO> obtenirFicheDePaieParSalaireBrutPlusGrandQue(int salaireBrut) ;

    public List<FicheDePaieDTO> obtenirFicheDePaieParSalaireBrutPlusPetitQue(int salaireBrut) ;

    public FicheDePaieDTO obtenirDerniereFicheDePaieParEmploye(int idEmploye) throws IllegalArgumentException;

    public void supprimerFicheDePaieParEmploye(int idEmploye) ;
}
