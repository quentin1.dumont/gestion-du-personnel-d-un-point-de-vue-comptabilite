package convertisseurs;

import dtos.FicheDePaieDTO;
import entities.FicheDePaieEntity;

import java.util.ArrayList;
import java.util.List;

public class ConvertisseurFicheDePaie {
    private static ConvertisseurFicheDePaie instance;

    private ConvertisseurFicheDePaie() {}

    public static ConvertisseurFicheDePaie getInstance() {
        if (instance == null) {
            instance = new ConvertisseurFicheDePaie();
        }
        return instance;
    }

    public FicheDePaieEntity convertirVersEntity(FicheDePaieDTO ficheDePaie) {
        FicheDePaieEntity ficheDePaieEntity = new FicheDePaieEntity();
        ficheDePaieEntity.setIdFicheDePaie(ficheDePaie.getIdFicheDePaie());
        ficheDePaieEntity.setDate(ficheDePaie.getDate());
        ficheDePaieEntity.setCotisationsPatronales(ficheDePaie.getCotisationsPatronales());
        ficheDePaieEntity.setIdEmploye(ficheDePaie.getIdEmploye());
        ficheDePaieEntity.setSalaireBrut(ficheDePaie.getSalaireBrut());
        ficheDePaieEntity.setRole(ficheDePaie.getRole());
        return ficheDePaieEntity;
    }

    public List<FicheDePaieEntity> convertirVersEntity(List<FicheDePaieDTO> ficheDePaieList) {
        List<FicheDePaieEntity> ficheDePaieEntityList = new ArrayList<>();
        for (FicheDePaieDTO ficheDePaie : ficheDePaieList) {
            ficheDePaieEntityList.add(convertirVersEntity(ficheDePaie));
        }
        return ficheDePaieEntityList;
    }

    public FicheDePaieDTO convertirVersDTO(FicheDePaieEntity ficheDePaie) {
        FicheDePaieDTO ficheDePaieDTO = new FicheDePaieDTO();
        ficheDePaieDTO.setIdFicheDePaie(ficheDePaie.getIdFicheDePaie());
        ficheDePaieDTO.setDate(ficheDePaie.getDate());
        ficheDePaieDTO.setCotisationsPatronales(ficheDePaie.getCotisationsPatronales());
        ficheDePaieDTO.setIdEmploye(ficheDePaie.getIdEmploye());
        ficheDePaieDTO.setSalaireBrut(ficheDePaie.getSalaireBrut());
        ficheDePaieDTO.setRole(ficheDePaie.getRole());
        return ficheDePaieDTO;
    }

    public List<FicheDePaieDTO> convertirVersDTO(List<FicheDePaieEntity> ficheDePaieList) {
        List<FicheDePaieDTO> ficheDePaieDTOList = new ArrayList<>();
        for (FicheDePaieEntity ficheDePaie : ficheDePaieList) {
            ficheDePaieDTOList.add(convertirVersDTO(ficheDePaie));
        }
        return ficheDePaieDTOList;
    }
}
