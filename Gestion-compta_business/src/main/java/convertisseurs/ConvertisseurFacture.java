package convertisseurs;

import dtos.FactureDTO;
import entities.FactureEntity;

import java.util.ArrayList;
import java.util.List;

public class ConvertisseurFacture {
    private static ConvertisseurFacture instance;

    private ConvertisseurFacture() {}

    public static ConvertisseurFacture getInstance() {
        if (instance == null) {
            instance = new ConvertisseurFacture();
        }
        return instance;
    }

    public FactureEntity convertirVersEntity(FactureDTO facture) {
        FactureEntity factureEntity = new FactureEntity();
        factureEntity.setIdFacture(facture.getIdFacture());
        factureEntity.setEmetteur(facture.getEmetteur());
        factureEntity.setPrix(facture.getPrix());
        factureEntity.setDate(facture.getDate());
        factureEntity.setDescription(facture.getDescription());
        return factureEntity;
    }

    public List<FactureEntity> convertirVersEntity(List<FactureDTO> factureList) {
        List<FactureEntity> factureEntityList = new ArrayList<>();
        for (FactureDTO facture : factureList) {
            factureEntityList.add(convertirVersEntity(facture));
        }
        return factureEntityList;
    }

    public FactureDTO convertirVersDTO(FactureEntity facture) {
        FactureDTO factureDTO = new FactureDTO();
        factureDTO.setIdFacture(facture.getIdFacture());
        factureDTO.setEmetteur(facture.getEmetteur());
        factureDTO.setPrix(facture.getPrix());
        factureDTO.setDate(facture.getDate());
        factureDTO.setDescription(facture.getDescription());
        return factureDTO;
    }

    public List<FactureDTO> convertirVersDTO(List<FactureEntity> factureList) {
        List<FactureDTO> factureDTOList = new ArrayList<>();
        for (FactureEntity facture : factureList) {
            factureDTOList.add(convertirVersDTO(facture));
        }
        return factureDTOList;
    }
}
